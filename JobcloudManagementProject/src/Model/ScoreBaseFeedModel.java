package Model;

public class ScoreBaseFeedModel {
	String name = "";
	Double score = 0.0;
	Double cliksPercentage = 0.0;
	Double jobSentPercentage = 0.0;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Double getCliksPercentage() {
		return cliksPercentage;
	}

	public void setCliksPercentage(Double cliksPercentage) {
		this.cliksPercentage = cliksPercentage;
	}

	public Double getJobSentPercentage() {
		return jobSentPercentage;
	}

	public void setJobSentPercentage(Double jobSentPercentage) {
		this.jobSentPercentage = jobSentPercentage;
	}

}
