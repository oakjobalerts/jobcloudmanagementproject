package Model;

import java.io.IOException;
import java.io.Writer;

import com.sun.xml.bind.marshaller.CharacterEscapeHandler;

public class EscapeHandler implements CharacterEscapeHandler {

    public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
        for (int i = start; i < start + length; i++) {
            char char1 = ch[i];
            out.write(char1);
        }

    }

}
