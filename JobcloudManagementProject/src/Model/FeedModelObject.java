package Model;

public class FeedModelObject {

	public int feedSource = 0;
	public String feedSourceName = "";
	public String feedDisplaySourceName = "";
	public int getFeedSource() {
		return feedSource;
	}
	public void setFeedSource(int feedSource) {
		this.feedSource = feedSource;
	}
	public String getFeedSourceName() {
		return feedSourceName;
	}
	public void setFeedSourceName(String feedSourceName) {
		this.feedSourceName = feedSourceName;
	}
	public String getFeedDisplaySourceName() {
		return feedDisplaySourceName;
	}
	public void setFeedDisplaySourceName(String feedDisplaySourceName) {
		this.feedDisplaySourceName = feedDisplaySourceName;
	}

}
