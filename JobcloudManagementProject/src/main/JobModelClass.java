package main;

public class JobModelClass {
	// here we declare the fields
	public String INDEX_CITY = "";
	public String INDEX_STATE = "";
	public String INDEX_ID = "";
	public String INDEX_JOB_URL = "";
	public String INDEX_LOCATION = "";
	public String INDEX_EMPLOYER = "";
	public String INDEX_TITLE = "";
	public String INDEX_TITLENEW = "";
	public String INDEX_POSTING_DATE = "";
	public String INDEX_ZIPCODE = "";
	public String INDEX_SOURCE = "";
	public String INDEX_SOURCENAME = "";
	public String INDEX_DISPLAY_SOURCENAME = "";
	public String INDEX_TITLE_THIRD = "";
	public String INDEX_DESCRIPTION_JOB = "";
	public String OAKINDEX_SOURCE = "";
	public String INDEX_LATITUDE = "";
	public String INDEX_LONGITUDE = "";
	public String INDEX_CPC = "";
	public String INDEX_GROSS_CPC = "";
	public String INDEX_FEED_SCORE = "";
	public String INDEX_JOB_COUNTRY = "";

}
