package main;

import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import net.spy.memcached.MemcachedClient;
import aws.services.cloudsearchv2.AmazonCloudSearchClient;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudsearchv2.model.DescribeDomainsResult;
import com.mysql.jdbc.Statement;

public class JobCloudMainClass {

	static int CLEARFIT_SOURCE = 0;
	static int CR_SOURCE1 = 0;
	static int CR_SOURCE2 = 0;

	static int CLEARFIT_JOBS_COUNT = 0;
	static int CR_JOBS_COUNT1 = 0;
	static int CR_JOBS_COUNT2 = 0;
	static int CR_JOBS_COUNT3 = 0;

	public static String activityName = "Data_Population";

	static {
		try {
			Utility.memcacheKey = "domain_endpoint_" + Utility.dateFormat.format(new Date());
			Utility.credentials = new BasicAWSCredentials(Utility.awsAccessKey, Utility.awsSecretKey);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String ar[]) {
		try {

			// =============================================//=================================================

			Utility.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded!!!!!!!!!! in rds");
		} catch (Exception e) {
			System.out.println(e);
		}

		// reading the jobs table name from the run time parameter
		Utility.writeNumberOfProcessCompleted(Utility.init);

		if (ar != null && ar.length > 0) {

			Connection con = null;
			Statement st = null;
			try {
				con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
				st = (Statement) con.createStatement();
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			for (int i = 0; i < ar.length; i++) {

				if (ar[i].toString().equalsIgnoreCase("test")) {
					Utility.isThisTestrun = true;
					continue;
				}
				String query = "select count(*) AS rowcount from " + ar[i] + " t1 left join tbl_zipcodes t2 on t1.zipcode=t2.zip";

				try {
					System.out.println("conection=" + con);
					// total jobs count
					System.out.println(query);
					ResultSet res = st.executeQuery(query);
					res.next();
					double data = res.getInt("rowcount");
					System.out.println("TOTAL DATA=" + data);
					Utility.tabel_count_map.put(ar[i], data);

					// ===================================//========================================

				} catch (SQLException e) {
					// TODO Auto-generated catch block
				}

			}
			try {
				if (con != null)
					con.close();
				if (st != null)
					st.close();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			System.out.println("reading scroe based feed model from file");

			Utility.read_score_base_json_from_file();
			Utility.readDocumentEndPointFromFile(Utility.tempQueueName);

			System.out.println("process has been started \n document end point=" + Utility.documentEndpoint);

			System.out.println(Utility.searchEndpoint);

			String domainName = Utility.domainName + Utility.dateFormat.format(new Date());
			String querys = "update " + Utility.activityTable + " set data_uploaded='" + Utility.dataInsertedInDomain + "',data_uploading_status='uploading' where domain_name='" + domainName + "'";

			try {
				boolean isInserted = Utility.insertDataIntoActivityStats("Data population has been started in " + domainName, activityName, "0");

				if (isInserted) {

					System.out.println("inserted successfully");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// =====================================================checking
			// status===========================
			boolean isDomainActive = getDomainStaus(domainName);
			if (!isDomainActive) {
				Utility.sendEmail("" + domainName + " has some issue", 0, "problem");

				Utility.sendTextSms(domainName + " has some issue");
			}
			startUploadingData();
		} else {
			Utility.sendTextSms("No jobs table name found from dataUploading process");
			System.exit(0);

		}

	}

	public static void startUploadingData() {
		// Amount of data needs to be processed....................
		// per thread data

		for (Map.Entry<String, Double> entry : Utility.tabel_count_map.entrySet()) {

			try {
				Utility.sendEmail("Data population has been started in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + entry.getKey(), entry.getValue(), "uploading");
				Utility.sendTextSms("Data population has been started in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " data=" + entry.getValue() + " Using Table=" + entry.getKey());

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(entry.getKey() + "/" + entry.getValue());

			if (!Utility.isThisTestrun && entry.getValue() > 1500000) {
				startProcessing(entry.getKey(), entry.getValue());
			} else {
				if (!Utility.isThisTestrun) {
					Utility.sendTextSms("Tbl_all_jobs has 1500000 data in the db. Please check this");
					System.exit(0);

				} else {
					System.out.println("its test run");
					startProcessing(entry.getKey(), entry.getValue());
				}
			}

		}
		System.out.println("total number of threads=" + Utility.totalNumberOfhread);

	}

	// getting status of domain wheheteher it is active or not
	public static boolean getDomainStaus(String domainName) {

		AmazonCloudSearchClient client = null;
		client = new AmazonCloudSearchClient(Utility.credentials);
		client.setRegion(Utility.region);
		DescribeDomainsResult describedomainResult = client.describeDomains();
		ArrayList<com.amazonaws.services.cloudsearchv2.model.DomainStatus> domainList = (ArrayList<com.amazonaws.services.cloudsearchv2.model.DomainStatus>) describedomainResult.getDomainStatusList();

		for (int i = 0; i < domainList.size(); i++) {
			if (domainList.get(i).getDomainName().equalsIgnoreCase(domainName.trim())) {
				if (!domainList.get(i).getProcessing()) {

					return true;

				}
			}
		}
		return false;
	}

	public static void startProcessing(String table, Double totalData) {

		Double amountOfData = (double) (totalData / Utility.tempNumberOfThread_perTable);
		int fromIndex = 0;
		int uptoWhichRecord = (int) (fromIndex + Math.ceil(amountOfData));
		System.out.println("Threads are going to create");
		for (int i = 0; i < Utility.tempNumberOfThread_perTable; i++) {
			Utility.totalNumberOfhread = Utility.totalNumberOfhread + 1;
			System.out.println("data for this thread=" + fromIndex + "," + uptoWhichRecord);
			// initiating thread
			DataPopulatinThread thread = new DataPopulatinThread(fromIndex, uptoWhichRecord, 6000, table);
			fromIndex = (int) (fromIndex + Math.ceil(amountOfData));
			uptoWhichRecord = (int) (uptoWhichRecord + Math.ceil(amountOfData));
		}

	}
}
