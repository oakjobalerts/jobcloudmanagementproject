package main;

public class IndexConstants {

	/**
	 * Index fields Name
	 */

	public static String INDEX_CITY = "city";
	public static String INDEX_STATE = "state";
	public static String INDEX_ID = "id";
	public static String INDEX_JOB_URL = "joburl";
	public static String INDEX_LOCATION = "location";
	public static String INDEX_EMPLOYER = "employer";
	public static String INDEX_TITLE = "title";
	public static String INDEX_TITLENEW = "titlenew";
	public static String INDEX_POSTING_DATE = "postingdate";
	public static String INDEX_ZIPCODE = "zipcode";
	public static String INDEX_SOURCE = "source";
	public static String INDEX_SOURCENAME = "sourcename";
	public static String INDEX_DISPLAY_SOURCENAME = "displaysource";
	public static String INDEX_TITLE_THIRD = "thirdtitle";
	public static String INDEX_DESCRIPTION_JOB = "description";
	public static String OAKINDEX_SOURCE = "oaksource";
	public static String OAKINDEX_CPC = "cpc";
	public static String OAKINDEX_GROSS_CPC = "gross_cpc";
	public static String OAKINDEX_FEED_SCORE = "feed_score";
	public static String OAKINDEX_JOB_COUNTRY = "job_country";

}
