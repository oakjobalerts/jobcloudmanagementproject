package main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import aws.services.cloudsearchv2.AmazonCloudSearchClient;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudsearchv2.model.CreateDomainRequest;
import com.amazonaws.services.cloudsearchv2.model.CreateDomainResult;
import com.amazonaws.services.cloudsearchv2.model.DateOptions;
import com.amazonaws.services.cloudsearchv2.model.DefineIndexFieldRequest;
import com.amazonaws.services.cloudsearchv2.model.DescribeDomainsResult;
import com.amazonaws.services.cloudsearchv2.model.DoubleOptions;
import com.amazonaws.services.cloudsearchv2.model.IndexDocumentsRequest;
import com.amazonaws.services.cloudsearchv2.model.IndexField;
import com.amazonaws.services.cloudsearchv2.model.IndexFieldType;
import com.amazonaws.services.cloudsearchv2.model.IntOptions;
import com.amazonaws.services.cloudsearchv2.model.LatLonOptions;
import com.amazonaws.services.cloudsearchv2.model.LiteralOptions;
import com.amazonaws.services.cloudsearchv2.model.PartitionInstanceType;
import com.amazonaws.services.cloudsearchv2.model.ScalingParameters;
import com.amazonaws.services.cloudsearchv2.model.TextOptions;
import com.amazonaws.services.cloudsearchv2.model.UpdateScalingParametersRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONObject;

public class CreateDomain {

	public static String domainName = "";
	public static AmazonCloudSearchClient client = null;
	private static CreateDomainResult domainResult = null;
	Timer timer;
	MyTimerTask mytimerTask;
	int count = 0;
	boolean isRequiedIndexing = true;
	boolean hasChangedPolicies = true;
	public static String analysisSchemeName = "synonyms";
	UpdateScalingParametersRequest updateScalingParametersRequest;
	com.amazonaws.services.cloudsearchv2.model.UpdateServiceAccessPoliciesRequest updateAccessPolicy;

	public static String activityName = "Domain_Activity";

	static int maxnumberTries = 10;
	static int currentNumberofTry = 0;
	static {
		try {
			Utility.credentials = new BasicAWSCredentials(Utility.awsAccessKey, Utility.awsSecretKey);
			// String sourceResults = Utility.getContent(Utility.sourceUrl);
			// if (sourceResults != null && sourceResults.length() > 0) {
			// Utility.createSoucesList(sourceResults);
			// }

		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. " + "Please make sure that your credentials file is at the correct " + "location (/home/signity/.aws/credentials), and is in valid format.", e);
		}

		try {
			System.out.println("Loading driver...");
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded!");

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Cannot find the driver in the classpath!", e);
		}

	}

	public static void main(String[] args) {

		System.out.println("domain creation");
		client = new AmazonCloudSearchClient(Utility.credentials);
		client.setRegion(Utility.region);

		try {
			CreateDomain cd = new CreateDomain();
			cd.createDomainWithIndex();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void updatePolicyAndScalingParameter() {

		// creting scaling parameter
		ScalingParameters scalingParameter = new ScalingParameters();
		scalingParameter.setDesiredInstanceType(PartitionInstanceType.SearchM32xlarge);
		scalingParameter.setDesiredReplicationCount(3);
		scalingParameter.setDesiredPartitionCount(1);

		updateScalingParametersRequest = new UpdateScalingParametersRequest();
		updateScalingParametersRequest.setDomainName(domainName);
		updateScalingParametersRequest.setRequestCredentials(Utility.credentials);
		updateScalingParametersRequest.setScalingParameters(scalingParameter);

		// upating accesspolicy to the domain.
		updateAccessPolicy = new com.amazonaws.services.cloudsearchv2.model.UpdateServiceAccessPoliciesRequest();
		updateAccessPolicy.setDomainName(domainName);
		updateAccessPolicy.setRequestCredentials(Utility.credentials);

		// create policy

		JSONObject jo = new JSONObject();

		try {

			jo.put("Version", "2012-10-17");

			JSONArray ja = new JSONArray();
			JSONObject jo1 = new JSONObject();
			jo1.put("Sid", "");
			jo1.put("Effect", "Allow");

			// principal
			JSONObject jo2 = new JSONObject();
			jo2.put("AWS", "*");
			jo1.put("Principal", jo2);

			jo1.put("Action", "cloudsearch:*");

			ja.put(jo1);
			jo.put("Statement", ja);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		// System.out.println(jo.toString());
		updateAccessPolicy.setAccessPolicies(jo.toString());

	}

	private void runIndexing() {

		IndexDocumentsRequest indexRequest = new IndexDocumentsRequest();
		// indexRequest.setDomainName(domainResult.getDomainStatus().getARN());
		indexRequest.setDomainName(domainName);
		client.indexDocuments(indexRequest);

	}

	private void createDomainWithIndex() {
		Scanner scan = new Scanner(System.in);
		domainName = Utility.domainName + Utility.dateFormat.format(Calendar.getInstance().getTime());

		if (Utility.isThisTestrun) {
			domainName = "test" + domainName;
		}

		CreateDomainRequest domainRequest = new CreateDomainRequest();
		domainRequest.setDomainName(domainName);
		domainRequest.setRequestCredentials(Utility.credentials);

		try {
			domainResult = client.createDomain(domainRequest);
			System.out.println("Domain creation request send");
			// upating accesspolicy to the domain.
			if (domainResult != null) {

				try {
					boolean isInserted = Utility.insertDataIntoActivityStats(domainName + " is going to create.", activityName, "0");

					if (isInserted) {

						System.out.println("inserted successfully");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			updatePolicyAndScalingParameter();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Request to create domain is having some error");
		}

		try {
			UploadRelationkeyWords.uploadRelationKeys();
			// to add aliases................
			// UploadRelationkeyWords.uploadRelationaliases();
			Thread.sleep(10000);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// create indexing ...

		// id
		DefineIndexFieldRequest fieldRequest1 = new DefineIndexFieldRequest();
		IndexField indexField1 = new IndexField();
		indexField1.setIndexFieldName(IndexConstants.INDEX_ID);
		indexField1.setIndexFieldType(IndexFieldType.Text);

		// IntOptions idOptions = new IntOptions();
		// idOptions.setSearchEnabled(true);
		// idOptions.setSortEnabled(true);
		// idOptions.setReturnEnabled(true);

		TextOptions idOption = new TextOptions();
		// cityOptions.setSortEnabled(true);
		idOption.setReturnEnabled(true);
		indexField1.setTextOptions(idOption);

		// indexField1.setIntOptions(idOptions);
		// defining field request......
		fieldRequest1.setIndexField(indexField1);
		fieldRequest1.setDomainName(domainName);
		try {
			client.defineIndexField(fieldRequest1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// city --text
		DefineIndexFieldRequest fieldRequest2 = new DefineIndexFieldRequest();
		IndexField indexField2 = new IndexField();
		indexField2.setIndexFieldName(IndexConstants.INDEX_CITY);
		indexField2.setIndexFieldType(IndexFieldType.Text);

		TextOptions cityOptions = new TextOptions();
		// cityOptions.setSortEnabled(true);
		cityOptions.setReturnEnabled(true);
		indexField2.setTextOptions(cityOptions);

		fieldRequest2.setIndexField(indexField2);
		fieldRequest2.setDomainName(domainName);

		try {
			client.defineIndexField(fieldRequest2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// state --text
		DefineIndexFieldRequest fieldRequest3 = new DefineIndexFieldRequest();
		IndexField indexField3 = new IndexField();
		indexField3.setIndexFieldName(IndexConstants.INDEX_STATE);
		indexField3.setIndexFieldType(IndexFieldType.Text);

		TextOptions stateOptions = new TextOptions();
		// stateOptions.setSortEnabled(true);
		stateOptions.setReturnEnabled(true);
		indexField3.setTextOptions(stateOptions);

		fieldRequest3.setIndexField(indexField3);
		fieldRequest3.setDomainName(domainName);
		try {
			client.defineIndexField(fieldRequest3);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// employer --text
		DefineIndexFieldRequest fieldRequest4 = new DefineIndexFieldRequest();
		IndexField indexField4 = new IndexField();
		indexField4.setIndexFieldName(IndexConstants.INDEX_EMPLOYER);
		indexField4.setIndexFieldType(IndexFieldType.Text);

		TextOptions empOptions = new TextOptions();
		// empOptions.setSortEnabled(true);
		empOptions.setReturnEnabled(true);
		indexField4.setTextOptions(empOptions);

		fieldRequest4.setIndexField(indexField4);
		fieldRequest4.setDomainName(domainName);
		try {
			client.defineIndexField(fieldRequest4);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// joburl --text
		DefineIndexFieldRequest fieldRequest5 = new DefineIndexFieldRequest();
		IndexField indexField5 = new IndexField();
		indexField5.setIndexFieldName(IndexConstants.INDEX_JOB_URL);
		indexField5.setIndexFieldType(IndexFieldType.Text);

		TextOptions jobOptions = new TextOptions();
		// jobOptions.setSortEnabled(true);
		jobOptions.setReturnEnabled(true);
		indexField5.setTextOptions(jobOptions);

		fieldRequest5.setIndexField(indexField5);
		fieldRequest5.setDomainName(domainName);
		try {
			client.defineIndexField(fieldRequest5);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// location --latlon
		DefineIndexFieldRequest fieldRequest6 = new DefineIndexFieldRequest();
		IndexField indexField6 = new IndexField();
		indexField6.setIndexFieldName(IndexConstants.INDEX_LOCATION);
		indexField6.setIndexFieldType(IndexFieldType.Latlon);

		LatLonOptions latlonOptions = new LatLonOptions();
		latlonOptions.setSearchEnabled(true);
		latlonOptions.setSortEnabled(true);
		latlonOptions.setReturnEnabled(true);
		indexField6.setLatLonOptions(latlonOptions);

		fieldRequest6.setIndexField(indexField6);
		fieldRequest6.setDomainName(domainName);
		try {
			client.defineIndexField(fieldRequest6);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// title -- text
		DefineIndexFieldRequest fieldRequest7 = new DefineIndexFieldRequest();
		IndexField indexField7 = new IndexField();
		indexField7.setIndexFieldName(IndexConstants.INDEX_TITLE);
		indexField7.setIndexFieldType(IndexFieldType.Literal);

		com.amazonaws.services.cloudsearchv2.model.LiteralOptions titleOptions = new com.amazonaws.services.cloudsearchv2.model.LiteralOptions();
		titleOptions.setReturnEnabled(true);
		indexField7.setLiteralOptions(titleOptions);

		fieldRequest7.setIndexField(indexField7);
		fieldRequest7.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest7);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DefineIndexFieldRequest fieldRequest = new DefineIndexFieldRequest();
		IndexField indexField = new IndexField();
		indexField.setIndexFieldName(IndexConstants.INDEX_TITLENEW);
		indexField.setIndexFieldType(IndexFieldType.Text);

		TextOptions titleOption = new TextOptions();
		titleOption.setAnalysisScheme(UploadRelationkeyWords.scheme.getAnalysisSchemeName());
		titleOption.setReturnEnabled(true);
		indexField.setTextOptions(titleOption);
		fieldRequest.setIndexField(indexField);
		fieldRequest.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// zipcode -- text
		DefineIndexFieldRequest fieldRequest8 = new DefineIndexFieldRequest();
		IndexField indexField8 = new IndexField();
		indexField8.setIndexFieldName(IndexConstants.INDEX_ZIPCODE);
		indexField8.setIndexFieldType(IndexFieldType.Text);

		TextOptions zipOptions = new TextOptions();
		// zipOptions.setSortEnabled(true);
		zipOptions.setReturnEnabled(true);
		indexField8.setTextOptions(zipOptions);

		fieldRequest8.setIndexField(indexField8);
		fieldRequest8.setDomainName(domainName);
		try {
			client.defineIndexField(fieldRequest8);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// source -- int
		DefineIndexFieldRequest fieldRequest9 = new DefineIndexFieldRequest();
		IndexField indexField9 = new IndexField();
		indexField9.setIndexFieldName(IndexConstants.INDEX_SOURCE);
		indexField9.setIndexFieldType(IndexFieldType.Int);

		IntOptions sourceOptions = new IntOptions();
		sourceOptions.setSearchEnabled(true);
		sourceOptions.setSortEnabled(true);
		sourceOptions.setReturnEnabled(true);

		indexField9.setIntOptions(sourceOptions);

		fieldRequest9.setIndexField(indexField9);
		fieldRequest9.setDomainName(domainName);
		try {
			client.defineIndexField(fieldRequest9);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// postingdate -- date
		DefineIndexFieldRequest fieldRequest10 = new DefineIndexFieldRequest();
		IndexField indexField10 = new IndexField();
		indexField10.setIndexFieldName(IndexConstants.INDEX_POSTING_DATE);
		indexField10.setIndexFieldType(IndexFieldType.Date);

		DateOptions dateOptions = new DateOptions();
		dateOptions.setSearchEnabled(true);
		dateOptions.setSortEnabled(true);
		dateOptions.setReturnEnabled(true);

		indexField10.setDateOptions(dateOptions);
		// ////////////////////////////////

		fieldRequest10.setIndexField(indexField10);
		fieldRequest10.setDomainName(domainName);
		try {
			client.defineIndexField(fieldRequest10);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DefineIndexFieldRequest fieldRequest11 = new DefineIndexFieldRequest();
		IndexField indexField11 = new IndexField();

		indexField11.setIndexFieldName(IndexConstants.INDEX_SOURCENAME);
		indexField11.setIndexFieldType(IndexFieldType.Literal);

		LiteralOptions titleOptions11 = new LiteralOptions();
		titleOptions11.setReturnEnabled(true);
		indexField11.setLiteralOptions(titleOptions11);
		fieldRequest11.setIndexField(indexField11);

		fieldRequest11.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest11);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DefineIndexFieldRequest fieldRequest12 = new DefineIndexFieldRequest();
		IndexField indexField12 = new IndexField();
		indexField12.setIndexFieldName(IndexConstants.INDEX_TITLE_THIRD);
		indexField12.setIndexFieldType(IndexFieldType.Text);

		TextOptions titleOption12 = new TextOptions();
		titleOption12.setAnalysisScheme(UploadRelationkeyWords.scheme2.getAnalysisSchemeName());
		titleOption12.setReturnEnabled(true);
		indexField12.setTextOptions(titleOption12);
		fieldRequest12.setIndexField(indexField12);
		fieldRequest12.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest12);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DefineIndexFieldRequest fieldRequest13 = new DefineIndexFieldRequest();
		IndexField indexField13 = new IndexField();
		indexField13.setIndexFieldName(IndexConstants.INDEX_DISPLAY_SOURCENAME);
		indexField13.setIndexFieldType(IndexFieldType.Text);

		TextOptions titleOption13 = new TextOptions();
		titleOption13.setReturnEnabled(true);
		indexField13.setTextOptions(titleOption13);
		fieldRequest13.setIndexField(indexField13);
		fieldRequest13.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest13);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// FOR DESCRIPTIONS
		DefineIndexFieldRequest fieldRequest14 = new DefineIndexFieldRequest();
		IndexField indexField14 = new IndexField();
		indexField14.setIndexFieldName(IndexConstants.INDEX_DESCRIPTION_JOB);
		indexField14.setIndexFieldType(IndexFieldType.Text);

		TextOptions titleOption14 = new TextOptions();
		titleOption14.setReturnEnabled(true);
		indexField14.setTextOptions(titleOption14);
		fieldRequest14.setIndexField(indexField14);
		fieldRequest14.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest14);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// oaksource....................

		DefineIndexFieldRequest fieldRequest15 = new DefineIndexFieldRequest();
		IndexField indexField15 = new IndexField();
		indexField15.setIndexFieldName(IndexConstants.OAKINDEX_SOURCE);
		indexField15.setIndexFieldType(IndexFieldType.Int);

		IntOptions sourceOptions15 = new IntOptions();
		sourceOptions15.setSearchEnabled(true);
		sourceOptions15.setSortEnabled(true);
		sourceOptions15.setReturnEnabled(true);

		indexField15.setIntOptions(sourceOptions15);

		fieldRequest15.setIndexField(indexField15);
		fieldRequest15.setDomainName(domainName);

		try {
			client.defineIndexField(fieldRequest15);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// adding cpc in the domain

		// DefineIndexFieldRequest fieldRequest16 = new
		// DefineIndexFieldRequest();
		// IndexField indexField16 = new IndexField();
		// indexField16.setIndexFieldName(IndexConstants.OAKINDEX_CPC);
		// indexField16.setIndexFieldType(IndexFieldType.Text);
		//
		// TextOptions titleOption16 = new TextOptions();
		// titleOption16.setReturnEnabled(true);
		// indexField16.setTextOptions(titleOption16);
		// fieldRequest16.setIndexField(indexField16);
		// fieldRequest16.setDomainName(domainName);
		//
		// try {
		//
		// client.defineIndexField(fieldRequest16);
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// new code for cpc 11-08-2016
		DefineIndexFieldRequest fieldRequest16 = new DefineIndexFieldRequest();
		IndexField indexField16 = new IndexField();
		indexField16.setIndexFieldName(IndexConstants.OAKINDEX_CPC);
		indexField16.setIndexFieldType(IndexFieldType.Double);

		DoubleOptions doubleOption16 = new DoubleOptions();
		doubleOption16.setReturnEnabled(true);
		indexField16.setDoubleOptions(doubleOption16);
		fieldRequest16.setIndexField(indexField16);
		fieldRequest16.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest16);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// adding gross_cpc in the domain

		DefineIndexFieldRequest fieldRequest17 = new DefineIndexFieldRequest();
		IndexField indexField17 = new IndexField();
		indexField17.setIndexFieldName(IndexConstants.OAKINDEX_GROSS_CPC);
		indexField17.setIndexFieldType(IndexFieldType.Text);

		TextOptions titleOption17 = new TextOptions();
		titleOption17.setReturnEnabled(true);
		indexField17.setTextOptions(titleOption17);
		fieldRequest17.setIndexField(indexField17);
		fieldRequest17.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest17);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// if (Utility.isThisTestrun) {
		DefineIndexFieldRequest fieldRequest18 = new DefineIndexFieldRequest();
		IndexField indexField18 = new IndexField();
		indexField18.setIndexFieldName(IndexConstants.OAKINDEX_FEED_SCORE);
		indexField18.setIndexFieldType(IndexFieldType.Double);

		DoubleOptions doubleOption18 = new DoubleOptions();
		doubleOption18.setReturnEnabled(true);
		indexField18.setDoubleOptions(doubleOption18);
		fieldRequest18.setIndexField(indexField18);
		fieldRequest18.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest18);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// here we are adding the country
		DefineIndexFieldRequest fieldRequest19 = new DefineIndexFieldRequest();
		IndexField indexField19 = new IndexField();
		indexField19.setIndexFieldName(IndexConstants.OAKINDEX_JOB_COUNTRY);
		indexField19.setIndexFieldType(IndexFieldType.Text);

		TextOptions titleOption19 = new TextOptions();
		titleOption19.setReturnEnabled(true);
		indexField19.setTextOptions(titleOption19);
		fieldRequest19.setIndexField(indexField19);
		fieldRequest19.setDomainName(domainName);

		try {

			client.defineIndexField(fieldRequest19);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// }

		System.out.println("indexing done");
		mytimerTask = new MyTimerTask();
		timer = new Timer();
		timer.scheduleAtFixedRate(mytimerTask, 30000, 300000);

	}

	public class MyTimerTask extends TimerTask {

		@Override
		public void run() {
			// TODO Auto-generated method stub

			currentNumberofTry = currentNumberofTry + 1;

			if (currentNumberofTry > maxnumberTries) {
				Utility.sendTextSms("These is some problem with cloud search domain service:) ");
				Utility.sendEmail("Alert! Domin not Active Properly", 0, "problem");

			}
			DescribeDomainsResult describedomainResult = client.describeDomains();
			ArrayList<com.amazonaws.services.cloudsearchv2.model.DomainStatus> domainList = (ArrayList<com.amazonaws.services.cloudsearchv2.model.DomainStatus>) describedomainResult.getDomainStatusList();
			for (int i = 0; i < domainList.size(); i++) {
				if (domainList.get(i).getDomainName().equalsIgnoreCase(domainName.trim())) {
					if (!domainList.get(i).getProcessing()) {
						System.out.println(new Date().toString());
						try {
							if (domainList.get(i).getRequiresIndexDocuments()) {
								IndexDocumentsRequest indexRequest = new IndexDocumentsRequest();
								indexRequest.setDomainName(domainName);
								indexRequest.setRequestCredentials(Utility.credentials);
								client.indexDocuments(indexRequest);
								System.out.println("under indexing");
							} else {
								System.out.println("Domain is now active properly please upload your data");
								Utility.documentEndpoint = domainList.get(i).getDocService().getEndpoint().trim();
								Utility.searchEndpoint = domainList.get(i).getSearchService().getEndpoint().trim();
								client.setDocumentEndpoint(Utility.documentEndpoint);
								client.setSearchEndpoint(Utility.searchEndpoint);
								if (!Utility.isThisTestrun) {
									try {

										try {
											Utility.deleteOldMessageFromqueue("https://sqs.us-east-1.amazonaws.com/306640124653/" + Utility.tempQueueName);
											Thread.sleep(10000);
										} catch (Exception e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										try {
											Utility.writeDomainInfo(Utility.documentEndpoint + "|" + Utility.searchEndpoint, Utility.tempQueueName);
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										try {
											addDomainInfoToqueue();
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										// =======making requeust to delete the
										// older
										// domain==============================
										try {
											Utility.sendTextSms("Domain Name " + Utility.domainName + Utility.dateFormat.format(new Date()) + " has been active properly.");
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										try {
											boolean isInserted = Utility.insertDataIntoActivityStats(domainName + " has been active properly", activityName, "1");

											if (isInserted) {

												System.out.println("inserted successfully");
											}
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										try {
											Utility.sendEmail("Domain has been active properly.", 0, "active");
											System.out.println("Mail has been send");
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								}
								try {
									timer.cancel();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					} else {
						System.out.println("under processing........");
						if (domainList.get(i).getRequiresIndexDocuments()) {
							IndexDocumentsRequest indexRequest = new IndexDocumentsRequest();
							indexRequest.setDomainName(domainName);
							indexRequest.setRequestCredentials(Utility.credentials);
							client.indexDocuments(indexRequest);

						}
						if (hasChangedPolicies) {

							try {
								System.out.println("sending scaling request");
								client.updateScalingParameters(updateScalingParametersRequest);
								System.out.println("request send");
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							try {
								System.out.println("sending accesspolicy");
								client.updateServiceAccessPolicies(updateAccessPolicy);
								System.out.println("request send");
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							hasChangedPolicies = false;
						}
					}
				}
			}

		}

	}

	public static void addDomainInfoToqueue() {
		AmazonSQS sqs;
		sqs = new AmazonSQSClient(Utility.credentials);
		sqs.setRegion(Utility.region);
		CreateQueueRequest createQueueRequest = new CreateQueueRequest(Utility.tempQueueName);
		try {
			Map<String, String> requestQueMap = new HashMap<String, String>();
			requestQueMap.put("VisibilityTimeout", "0");
			createQueueRequest.setAttributes(requestQueMap);
			String queUrl = sqs.createQueue(createQueueRequest).getQueueUrl();
			System.out.println("que created and add info to the queue ");
			sqs.sendMessage(queUrl, Utility.documentEndpoint + "|" + Utility.searchEndpoint);
		} catch (AmazonServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public class SecondTimerClass extends TimerTask {

		@Override
		public void run() {
			// TODO Auto-generated method stub

		}

	}

}
