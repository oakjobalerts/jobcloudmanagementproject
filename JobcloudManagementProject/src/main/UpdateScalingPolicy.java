package main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import aws.services.cloudsearchv2.AmazonCloudSearchClient;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.cloudsearchv2.model.DescribeDomainsResult;
import com.amazonaws.services.cloudsearchv2.model.DomainStatus;
import com.amazonaws.services.cloudsearchv2.model.PartitionInstanceType;
import com.amazonaws.services.cloudsearchv2.model.ScalingParameters;
import com.amazonaws.services.cloudsearchv2.model.UpdateScalingParametersRequest;

public class UpdateScalingPolicy {
	static String domainName = "";
	static UpdateScalingParametersRequest updateScalingParametersRequest;
	static AmazonCloudSearchClient client = null;

	public UpdateScalingPolicy() {
	}

	public static void main(String[] ar) {
		try {
			Utility.credentials = new BasicAWSCredentials(Utility.awsAccessKey,
					Utility.awsSecretKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			Calendar cal = Calendar.getInstance();
			// cal.add(Calendar.DATE, -1);
			domainName = Utility.domainName
					+ Utility.dateFormat.format(cal.getTime());

			System.out.println(domainName);
			client = new AmazonCloudSearchClient(Utility.credentials);
			client.setRegion(Utility.region);
		} catch (IllegalArgumentException e2) {
			e2.printStackTrace();
		}

		try {
			updatePolicyAndScalingParameter();
		} catch (Exception e) {
			e.printStackTrace();
		}

		DescribeDomainsResult describedomainResult = client.describeDomains();
		ArrayList<DomainStatus> domainList = (ArrayList) describedomainResult
				.getDomainStatusList();
		for (int i = 0; i < domainList.size(); i++) {
			if (((DomainStatus) domainList.get(i)).getDomainName()
					.equalsIgnoreCase(domainName.trim())) {
				if (!((DomainStatus) domainList.get(i)).getProcessing()
						.booleanValue()) {
					System.out.println(new Date().toString());
					try {
						System.out.println("sending scaling request");
						client.updateScalingParameters(updateScalingParametersRequest);
						System.out.println("request send");

						Utility.sendEmail("Domain " + domainName
								+ " has been successfully de-scaled", 0.0,
								"descale");

						System.exit(0);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	private static void updatePolicyAndScalingParameter() {
		ScalingParameters scalingParameter = new ScalingParameters();
		scalingParameter
				.setDesiredInstanceType(PartitionInstanceType.SearchM32xlarge);
		scalingParameter.setDesiredReplicationCount(Integer.valueOf(1));

		updateScalingParametersRequest = new UpdateScalingParametersRequest();
		updateScalingParametersRequest.setDomainName(domainName);
		updateScalingParametersRequest
				.setRequestCredentials(Utility.credentials);
		updateScalingParametersRequest.setScalingParameters(scalingParameter);
	}
}
