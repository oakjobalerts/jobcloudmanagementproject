package main;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.amazonaws.services.cloudsearchv2.model.AlgorithmicStemming;
import com.amazonaws.services.cloudsearchv2.model.AnalysisOptions;
import com.amazonaws.services.cloudsearchv2.model.AnalysisScheme;
import com.amazonaws.services.cloudsearchv2.model.AnalysisSchemeLanguage;
import com.amazonaws.services.cloudsearchv2.model.DefineAnalysisSchemeRequest;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class UploadRelationkeyWords {
	static Connection con;
	static Statement st;
	static String strSynonyms = "";
	static AnalysisScheme scheme, scheme2;

	static {
		try {
			System.out.println("Loading driver...");
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver loaded!");
			System.out.println(Utility.url + " " + Utility.username + " " + Utility.password);
			try {
				con = (Connection) DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
				st = (Statement) con.createStatement();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Cannot find the driver in the classpath!", e);
		}

	}

	public static void main(String[] ar) {
		uploadRelationaliases();
	}

	public static void uploadRelationKeys() {
		Utility.keywordsList.clear();
		String query = "SELECT * FROM keywords";
		try {
			ResultSet rs = st.executeQuery(query);
			Keyword keywords = null;
			while (rs.next()) {
				keywords = new Keyword();
				keywords.setId(rs.getInt("id"));
				keywords.setKeyword(rs.getString("keyword"));
				keywords.setParent_id(rs.getInt("parent_id"));

				Utility.keywordsList.add(keywords);

			}

		} catch (SQLException e) {
		}

		JSONObject jo = new JSONObject();

		JSONArray synArray = new JSONArray();

		for (int i = 0; i < Utility.keywordsList.size(); i++) {

			ArrayList<String> arr = getRelationKeys(Utility.keywordsList.get(i).getId());

			if (arr.size() > 1) {

				JSONArray ja = new JSONArray();

				for (int j = 0; j < arr.size(); j++) {
					ja.put(arr.get(j));
				}

				synArray.put(ja);
			}
		}
		System.out.println("finish");

		try {
			jo.put("groups", synArray);
			jo.put("aliases", new JSONObject());

		} catch (Exception e) { // TODO: handle exception

		}

		strSynonyms = jo.toString();
		addSynonymToDomain();
		if (con != null)
			try {
				con.close();
			} catch (SQLException e1) { // TODO Auto-generated catch
				e1.printStackTrace();
			}

	}

	public static void uploadRelationaliases() {
		JSONObject mainSendingObject = new JSONObject();
		Utility.keywordsList.clear();
		String query = "SELECT * FROM keywords";
		System.out.println(query);
		try {
			ResultSet rs = st.executeQuery(query);
			Keyword keywords = null;
			while (rs.next()) {
				keywords = new Keyword();
				keywords.setId(rs.getInt("id"));
				keywords.setKeyword(rs.getString("keyword"));
				keywords.setParent_id(rs.getInt("parent_id"));
				Utility.keywordsList.add(keywords);

			}
			System.out.println("completed");

		} catch (SQLException e) {
		}

		JSONObject aliasMainObject = new JSONObject();
		for (int i = 0; i < Utility.keywordsList.size(); i++) {
			ArrayList<String> arr = getRelationKeys(Utility.keywordsList.get(i).getId());
			if (arr.size() > 1) {
				JSONArray ja = new JSONArray();
				// String[] ja = new String[arr.size()];
				for (int j = 0; j < arr.size(); j++) {
					// ja[j] = arr.get(j);
					ja.put(arr.get(j));
				}

				try {
					aliasMainObject.put(Utility.keywordsList.get(i).getKeyword(), ja);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			if (i >= 50)
				break;
		}
		System.out.println("loop finished");
	
		try {
			mainSendingObject.put("groups", new JSONArray());
			mainSendingObject.put("aliases", aliasMainObject);
		
		} catch (Exception e) {
			// TODO: handle exception
		}
		// System.out.println(aliasMainObject.toString());
		strSynonyms = mainSendingObject.toString();
		System.out.println(strSynonyms);
		// System.out.println(strSynonyms);
		addSynonymToDomain();
		if (con != null)
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

	public static ArrayList<String> getRelationKeys(int id) {
		ArrayList<String> relationsKeys = new ArrayList<String>();
		String query = "SELECT * FROM keywords" + " WHERE parent_id=" + id;
		try {
			ResultSet rs = st.executeQuery(query);
			// Keyword keywords = null;
			while (rs.next()) {
				relationsKeys.add(rs.getString("keyword"));

			}

		} catch (SQLException e) {
			// L.e("PushQueueDAO :: getKeywords() :: Exception " +
			// e.toString());
		}

		return relationsKeys;

	}

	public static void addSynonymToDomain() {
		scheme = new AnalysisScheme();
		AnalysisOptions option = new AnalysisOptions();
		option.setSynonyms(strSynonyms);
		scheme.setAnalysisSchemeName(CreateDomain.analysisSchemeName);
		scheme.setAnalysisOptions(option);
		scheme.setAnalysisSchemeLanguage(AnalysisSchemeLanguage.En);

		DefineAnalysisSchemeRequest defineAnalysisSchemeRequest = new DefineAnalysisSchemeRequest();
		defineAnalysisSchemeRequest.setDomainName(CreateDomain.domainName);
		defineAnalysisSchemeRequest.setAnalysisScheme(scheme);

		try {
			System.out.println("adding aliases");
			CreateDomain.client.defineAnalysisScheme(defineAnalysisSchemeRequest);
			System.out.println("aliases added success fully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		scheme2 = new AnalysisScheme();
		AnalysisOptions options = new AnalysisOptions();
		options.setAlgorithmicStemming(AlgorithmicStemming.None);
		scheme2.setAnalysisSchemeName("nostemming");
		scheme2.setAnalysisSchemeLanguage(AnalysisSchemeLanguage.En);
		scheme2.setAnalysisOptions(options);

		DefineAnalysisSchemeRequest defineAnalysisScheme = new DefineAnalysisSchemeRequest();
		defineAnalysisScheme.setDomainName(CreateDomain.domainName);
		defineAnalysisScheme.setAnalysisScheme(scheme2);

		try {
			CreateDomain.client.defineAnalysisScheme(defineAnalysisScheme);
			System.out.println("synonym add successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
