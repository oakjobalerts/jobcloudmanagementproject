package main;

import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import net.spy.memcached.MemcachedClient;
import aws.services.cloudsearchv2.AmazonCloudSearchClient;
import aws.services.cloudsearchv2.AmazonCloudSearchInternalServerException;
import aws.services.cloudsearchv2.AmazonCloudSearchRequestException;
import aws.services.cloudsearchv2.documents.AmazonCloudSearchAddRequest;

import com.amazonaws.util.json.JSONException;
import com.mysql.jdbc.Statement;

public class DataPopulatinThread extends Thread {

	// defining properties of thread object

	Thread thread;
	Statement st;
	// sql connection object
	Connection con;
	ResultSet res;
	int startingIndex, uptoWhichRecord, offset, lastIndex;
	String table_name;
	AmazonCloudSearchClient searchClient;
	ArrayList<AmazonCloudSearchAddRequest> requestList = new ArrayList<AmazonCloudSearchAddRequest>();
	String query = "";

	// Creating thread with following value as their property..............

	public DataPopulatinThread(int startingIndex, int numberOfRecords, int offset, String table_name) {

		this.startingIndex = startingIndex;
		this.uptoWhichRecord = numberOfRecords;
		this.offset = offset;
		this.lastIndex = this.startingIndex + this.offset;
		this.table_name = table_name;

		this.searchClient = new AmazonCloudSearchClient(Utility.credentials);
		this.searchClient.setSearchEndpoint(Utility.searchEndpoint);
		this.searchClient.setDocumentEndpoint(Utility.documentEndpoint);

		this.searchClient.setRegion(Utility.region);

		try {
			// creating connection with mysql...........
			if (this.con == null)
				this.con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			System.out.println("connected");
			this.st = (Statement) this.con.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.thread = new Thread(this);
		this.thread.setName("Thread " + Utility.threadName);
		Utility.threadName++;
		this.thread.start();

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		// method to populate data into cloudsearch
		populatingData_toCloud();
	}

	public void populatingData_toCloud() {

		try {
			try {
				// creating connection with mysql...........
				if (this.con == null)
					this.con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (this.con != null) {
				// inner join query to populate job records with lat long
				this.query = "select t1.id,t1.unique_job_id,t1.employer,t1.title,t1.postingdate,t1.joburl,t1.city,t1.state,t1.zipcode,t1.source,t1.description,t1.cpc,t1.source_name,t1.display_name,t1.gross_cpc,t2.latitude,t2.longitude,t1.job_country,t1.job_reference " + "from " + this.table_name + " t1 left join tbl_zipcodes t2 on t1.zipcode=t2.zip limit " + this.startingIndex + "," + this.offset;
				System.out.println(this.query);

				if (this.st == null)
					this.st = (Statement) this.con.createStatement();

				// data which will be sent to cloudsearch
				this.res = this.st.executeQuery(this.query);
				if (this.requestList.size() != 0)
					this.requestList.clear();
				if (this.res != null) {
					try {
						while (!this.res.isClosed() && this.res.next()) {

							// creating amazon search request object to add data
							// into the domain..............

							AmazonCloudSearchAddRequest addRequest = new AmazonCloudSearchAddRequest();
							JobModelClass job = new JobModelClass();
							try {

								addRequest.id = res.getString("id").trim();
								addRequest.version = 2;
							} catch (Exception e10) {
								// TODO Auto-generated catch block
								e10.printStackTrace();
							}
							try {
								// job.INDEX_ID = res.getString("id").trim();
								// addRequest.addField(IndexConstants.INDEX_ID,
								// Integer.parseInt(job.INDEX_ID));

								// here we are setting md5 unique job id in id on 28-11-2017
								job.INDEX_ID = res.getString("unique_job_id").trim();
								addRequest.addField(IndexConstants.INDEX_ID, job.INDEX_ID);
							} catch (NumberFormatException e9) {
								// TODO Auto-generated catch block
							}

							// setting source name and display sourcename
							try {

								job.INDEX_SOURCENAME = res.getString("source_name");
								addRequest.addField(IndexConstants.INDEX_SOURCENAME, job.INDEX_SOURCENAME);

							} catch (Exception e) {
								addRequest.addField(IndexConstants.INDEX_SOURCENAME, "");
							}

							// here we are modifying the document id and id of
							// job

							boolean isViktre_Careers = false;

							if (job.INDEX_SOURCENAME.equalsIgnoreCase("Viktre Careers") || job.INDEX_SOURCENAME.equalsIgnoreCase("VCN Single Job Posts")) {
								isViktre_Careers = true;

								try {

									addRequest.id = res.getString("job_reference").trim();
									addRequest.version = 2;
								} catch (Exception e10) {
									// TODO Auto-generated catch block
									e10.printStackTrace();
								}
								try {
									job.INDEX_ID = res.getString("job_reference").trim();

									addRequest.addField(IndexConstants.INDEX_ID, job.INDEX_ID);
								} catch (NumberFormatException e9) {
									// TODO Auto-generated catch block
									e9.printStackTrace();
								}

							}

							// code end here

							try {
								job.INDEX_EMPLOYER = res.getString("employer").trim();
								addRequest.addField(IndexConstants.INDEX_EMPLOYER, job.INDEX_EMPLOYER);
							} catch (Exception e8) {
								// TODO Auto-generated catch block
								e8.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_EMPLOYER, "");
							}
							try {
								job.INDEX_TITLE = res.getString("title").trim();

								addRequest.addField(IndexConstants.INDEX_TITLE, job.INDEX_TITLE);
							} catch (Exception e7) {
								// TODO Auto-generated catch block
								e7.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_TITLE, "");
							}

							try {

								job.INDEX_TITLENEW = res.getString("title").trim();
								addRequest.addField(IndexConstants.INDEX_TITLENEW, job.INDEX_TITLENEW);
							} catch (Exception e7) {
								// TODO Auto-generated catch block
								e7.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_TITLENEW, "");
							}

							try {

								job.INDEX_TITLE_THIRD = res.getString("title").trim();

								addRequest.addField(IndexConstants.INDEX_TITLE_THIRD, job.INDEX_TITLE_THIRD);
							} catch (Exception e7) {
								// TODO Auto-generated catch block
								e7.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_TITLE_THIRD, "");
							}

							// addRequest.addField("description",
							// res.getString("description"));
							try {
								if (res.getString("postingdate").trim().equalsIgnoreCase("")) {
									// System.out.println("date "+res.getString("postingdate"));
								} else {
									// System.out.println("date "+res.getString("postingdate"));
									String[] dateArray = res.getString("postingdate").split(" ");

									DateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
									DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");

									fromFormat.setLenient(false);

									toFormat.setLenient(false);
									Date date = fromFormat.parse(dateArray[0]);

									job.INDEX_POSTING_DATE = toFormat.format(date) + "T00:00:00Z";
									addRequest.addField(IndexConstants.INDEX_POSTING_DATE, job.INDEX_POSTING_DATE);
								}
							} catch (Exception e6) {
								// TODO Auto-generated catch block
								// e6.printStackTrace();
								DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
								Calendar cal = Calendar.getInstance();
								cal.add(Calendar.DATE, -1);
								String lastDate = toFormat.format(cal.getTime());

								addRequest.addField(IndexConstants.INDEX_POSTING_DATE, lastDate + "T00:00:00Z");
							}
							try {

								job.INDEX_JOB_URL = res.getString("joburl").trim();
								addRequest.addField(IndexConstants.INDEX_JOB_URL, job.INDEX_JOB_URL);
							} catch (Exception e5) {
								// TODO Auto-generated catch block
								e5.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_JOB_URL, "");
							}

							// getting source number
							try {
								job.INDEX_SOURCE = res.getString("source").trim();
								addRequest.addField(IndexConstants.INDEX_SOURCE, Integer.parseInt(job.INDEX_SOURCE));
							} catch (Exception e) {
								// TODO Auto-generated catch block
								// e.printStackTrace();
								System.out.println("surce inserting");
								addRequest.addField(IndexConstants.INDEX_SOURCE, 100);
							}
							// getting zipcodes
							try {

								job.INDEX_ZIPCODE = res.getString("zipcode").trim();
								addRequest.addField(IndexConstants.INDEX_ZIPCODE, job.INDEX_ZIPCODE);

							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_ZIPCODE, "");

							}

							// testing in only test domain
							// if (!Utility.isThisTestrun) {
							job.INDEX_FEED_SCORE = "0.0";
							try {
								Double score = Utility.scoreBaseFeedModel_map.get(job.INDEX_SOURCENAME);
								if (score != null) {
									job.INDEX_FEED_SCORE = "" + score;

								}

								addRequest.addField(IndexConstants.OAKINDEX_FEED_SCORE, job.INDEX_FEED_SCORE);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								addRequest.addField(IndexConstants.OAKINDEX_FEED_SCORE, "0.0");
							}
							// }
							// insert data in actual domain only
							try {
								job.INDEX_DESCRIPTION_JOB = res.getString("description").trim();
								// we have revert this code to show same style
								// in the desc page
								// job.INDEX_DESCRIPTION_JOB =
								// Utility.html2text(job.INDEX_DESCRIPTION_JOB);

								// if (job.INDEX_DESCRIPTION_JOB.length() > 400
								// && !isViktre_Careers) {
								// job.INDEX_DESCRIPTION_JOB =
								// job.INDEX_DESCRIPTION_JOB.substring(0, 400);
								// }
								addRequest.addField(IndexConstants.INDEX_DESCRIPTION_JOB, job.INDEX_DESCRIPTION_JOB);
							} catch (Exception e2) {
								// TODO Auto-generated catch block
								e2.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_DESCRIPTION_JOB, "");

							}

							try {

								job.INDEX_DISPLAY_SOURCENAME = res.getString("display_name");
								addRequest.addField(IndexConstants.INDEX_DISPLAY_SOURCENAME, job.INDEX_DISPLAY_SOURCENAME);

							} catch (Exception e) {
								addRequest.addField(IndexConstants.INDEX_DISPLAY_SOURCENAME, "");
							}

							// this is canada feeds

							if (job.INDEX_DISPLAY_SOURCENAME.toLowerCase().contains("canada") || job.INDEX_DISPLAY_SOURCENAME.toLowerCase().contains("linkus")

							) {

								job = Utility.getzipcodeLatLongFor_TheCanadaFeeds(job);

							}
							// this is for uk feeds.

							if (job.INDEX_DISPLAY_SOURCENAME.toLowerCase().contains(" uk")) {

								job = Utility.getzipcodeLatLongFor_The_UKFeeds(job);

							}

							try {
								if (job.INDEX_CITY.equalsIgnoreCase("")) {
									job.INDEX_CITY = res.getString("city").trim();
								}

								if (job.INDEX_CITY == null || job.INDEX_CITY.toLowerCase().contains("null")) {
									addRequest.addField(IndexConstants.INDEX_CITY, "");
								} else {
									job.INDEX_CITY = job.INDEX_CITY;
									addRequest.addField(IndexConstants.INDEX_CITY, job.INDEX_CITY);
								}

							} catch (Exception e4) {
								// TODO Auto-generated catch block
								e4.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_CITY, "");
							}
							try {
								if (job.INDEX_STATE.equalsIgnoreCase("")) {
									job.INDEX_STATE = res.getString("state").trim();

								}

								if (job.INDEX_STATE == null || job.INDEX_STATE.toLowerCase().contains("null")) {
									addRequest.addField(IndexConstants.INDEX_STATE, "");
								} else {
									job.INDEX_STATE = job.INDEX_STATE;
									addRequest.addField(IndexConstants.INDEX_STATE, job.INDEX_STATE);
								}

							} catch (Exception e3) {
								// TODO Auto-generated catch block
								e3.printStackTrace();
								addRequest.addField(IndexConstants.INDEX_STATE, "");
							}

							// getting lat and long for the job

							try {

								if (job.INDEX_LATITUDE.equalsIgnoreCase("") || job.INDEX_LONGITUDE.equalsIgnoreCase("")) {
									job.INDEX_LATITUDE = res.getString("latitude").trim();
									job.INDEX_LONGITUDE = res.getString("longitude").trim();

								}

								if (job.INDEX_LATITUDE == null || job.INDEX_LONGITUDE == null) {
									addRequest.addField(IndexConstants.INDEX_LOCATION, "0" + "," + "0");

								} else if (job.INDEX_LATITUDE.toLowerCase().contains("null") || job.INDEX_LONGITUDE.toLowerCase().contains("null")) {
									addRequest.addField(IndexConstants.INDEX_LOCATION, "0" + "," + "0");
								} else {
									addRequest.addField(IndexConstants.INDEX_LOCATION, job.INDEX_LATITUDE + "," + job.INDEX_LONGITUDE);

								}

							} catch (Exception e1) {
								// TODO Auto-generated catch block
								addRequest.addField(IndexConstants.INDEX_LOCATION, "0,0");
							}

							Integer oaksource = null;
							try {

								addRequest.addField(IndexConstants.OAKINDEX_SOURCE, job.INDEX_SOURCE);

							} catch (Exception e) {
								addRequest.addField(IndexConstants.OAKINDEX_SOURCE, "");
							}
							// adding cpc in the domain
							try {

								job.INDEX_CPC = res.getString("cpc").trim();

								// new code of cpc by 2016-8-11

								try {
									Double cpcValue = Double.parseDouble(job.INDEX_CPC);
									addRequest.addField(IndexConstants.OAKINDEX_CPC, String.valueOf(cpcValue));
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							} catch (Exception e7) {
								// TODO Auto-generated catch block
								System.out.println("cpc value not found in the database");
								addRequest.addField(IndexConstants.OAKINDEX_CPC, "");
							}
							// adding gross cpc
							try {

								job.INDEX_GROSS_CPC = res.getString("gross_cpc").trim();

								addRequest.addField(IndexConstants.OAKINDEX_GROSS_CPC, job.INDEX_GROSS_CPC);
							} catch (Exception e7) {
								// TODO Auto-generated catch block
								System.out.println("gross_cpc value not found in the database");
								addRequest.addField(IndexConstants.OAKINDEX_GROSS_CPC, "");
							}

							// adding gross cpc
							try {

								job.INDEX_JOB_COUNTRY = res.getString("job_country").trim();

								addRequest.addField(IndexConstants.OAKINDEX_JOB_COUNTRY, job.INDEX_JOB_COUNTRY);
							} catch (Exception e7) {

								addRequest.addField(IndexConstants.OAKINDEX_JOB_COUNTRY, "");
							}

							this.requestList.add(addRequest);
							addRequest = null;
							job = null;

							// here we are updating cloud after every 2000 jobs
							// record
							if (this.requestList.size() == 2000) {
								System.out.println("sending document");
								try {
									this.searchClient.addDocuments(this.requestList);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								System.out.println("document send");
								try {
									Utility.dataInsertedInDomain = Utility.dataInsertedInDomain + this.requestList.size();
									if (!Utility.isHalfUploaded) {

										halfDataIsUploaded(this.searchClient);
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								this.requestList.clear();
							}

						}

						if (this.requestList.size() != 0) {
							try {
								// System.out.println("entered into condition");
								System.out.println("sending document");
								System.out.println(this.thread.getName());
								this.searchClient.addDocuments(this.requestList);
								System.out.println("document send");

								Utility.dataInsertedInDomain = Utility.dataInsertedInDomain + this.requestList.size();
								this.requestList.clear();
								if (!Utility.isHalfUploaded) {

									halfDataIsUploaded(this.searchClient);
								}

								if (this.lastIndex >= this.uptoWhichRecord) {
									System.out.println("Thread completed named by" + this.getName());
									Utility.numberOfCompleteThreads = Utility.numberOfCompleteThreads + 1;
									if (Utility.numberOfCompleteThreads >= Utility.totalNumberOfhread) {
										if (!Utility.isThisTestrun) {
											try {
												// check that file all ready
												// written with the new record
												if (!Utility.is_new_domain_exist(Utility.actualFileName)) {
													Utility.writeDomainInfo(Utility.documentEndpoint + "|" + Utility.searchEndpoint, Utility.actualFileName);
												}
												Utility.deleteOldDomain(this.searchClient);
												try {
													Utility.sendTextSms("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()));
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													e.printStackTrace();
												}
												Utility.sendEmail("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, 0, "complete");
											} catch (Exception e1) {
												// TODO Auto-generated catch
												// block
												e1.printStackTrace();
											}
											// updating database
											try {
												boolean isInserted = Utility.insertDataIntoActivityStats("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, JobCloudMainClass.activityName, "1");

												if (isInserted) {

													System.out.println("inserted successfully");
												}
											} catch (Exception e1) {
												// TODO Auto-generated catch
												// block
												e1.printStackTrace();
											}

											try {
												Utility.deleteOldMessageFromqueue("https://sqs.us-east-1.amazonaws.com/306640124653/" + Utility.actualQueueName);

											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											// adding domain iformatgion in the
											// queue and the file
											// systema,,,,,,,,,,,,,,,,

											try {
												Utility.addDomainInfoToqueue();
												//

												if (Utility.memcacheObj == null)
													Utility.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));

												Utility.memcacheObj.set(Utility.memcacheKey, 0, Utility.searchEndpoint);
												System.out.println("Value set into the memcachekey=  " + Utility.memcacheKey);
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

											System.out.println("complete data by" + this.thread.getName());
											System.exit(0);
										}
									}

									// System.out.println("complete data by" +
									// this.thread.getName());

								} else {
									this.startingIndex = this.lastIndex;
									if (this.startingIndex + this.offset > this.uptoWhichRecord) {
										this.offset = this.uptoWhichRecord - this.lastIndex;
										this.lastIndex = this.startingIndex + this.offset;
									} else {
										this.lastIndex = this.startingIndex + this.offset;

									}

									this.res = null;
									populatingData_toCloud();
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block

								// TODO Auto-generated catch block
								e.printStackTrace();
								this.requestList.clear();
								if (this.lastIndex >= this.uptoWhichRecord) {
									System.out.println("Thread completed named by" + this.getName());
									Utility.numberOfCompleteThreads = Utility.numberOfCompleteThreads + 1;
									if (Utility.numberOfCompleteThreads >= Utility.totalNumberOfhread) {
										if (!Utility.isThisTestrun) {
											try {
												// check that file all ready
												// written with the new record
												if (!Utility.is_new_domain_exist(Utility.actualFileName)) {
													Utility.writeDomainInfo(Utility.documentEndpoint + "|" + Utility.searchEndpoint, Utility.actualFileName);
												}
												Utility.deleteOldDomain(this.searchClient);
												try {
													Utility.sendTextSms("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + "Using Table=" + this.table_name);
												} catch (Exception e1) {
													// TODO Auto-generated catch
													// block
													e1.printStackTrace();
												}
												Utility.sendEmail("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + "Using Table=" + this.table_name, 0, "complete");
											} catch (Exception e2) {
												// TODO Auto-generated catch
												// block
												e2.printStackTrace();
											}
											// updating database
											try {
												boolean isInserted = Utility.insertDataIntoActivityStats("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, JobCloudMainClass.activityName, "1");

												if (isInserted) {

													System.out.println("inserted successfully");
												}
											} catch (Exception e1) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

											try {
												Utility.deleteOldMessageFromqueue("https://sqs.us-east-1.amazonaws.com/306640124653/" + Utility.actualQueueName);

											} catch (Exception e1) {
												// TODO Auto-generated catch
												// block
												e1.printStackTrace();
											}
											Utility.addDomainInfoToqueue();
											if (Utility.memcacheObj == null)
												Utility.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
											Utility.memcacheObj.set(Utility.memcacheKey, 0, Utility.searchEndpoint);
											System.out.println("Value set into the memcachekey=  " + Utility.memcacheKey);
											System.out.println("complete data by" + this.thread.getName());

											System.exit(0);
										}
									}

									System.out.println("complete data by" + this.thread.getName());

								} else {
									System.out.println("query=" + this.query);
									this.startingIndex = this.startingIndex + 100;
									if (this.startingIndex + this.offset > this.uptoWhichRecord) {
										this.offset = this.uptoWhichRecord - this.lastIndex;
										this.lastIndex = this.startingIndex + this.offset;
									} else {
										this.lastIndex = this.startingIndex + this.offset;

									}

									this.res = null;
									populatingData_toCloud();
								}

							} catch (AmazonCloudSearchRequestException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								this.requestList.clear();

								if (this.lastIndex >= this.uptoWhichRecord) {
									System.out.println("Thread completed named by" + this.getName());
									Utility.numberOfCompleteThreads = Utility.numberOfCompleteThreads + 1;
									if (Utility.numberOfCompleteThreads >= Utility.totalNumberOfhread) {
										if (!Utility.isThisTestrun) {
											try {
												// check that file all ready
												// written with the new record
												if (!Utility.is_new_domain_exist(Utility.actualFileName)) {
													Utility.writeDomainInfo(Utility.documentEndpoint + "|" + Utility.searchEndpoint, Utility.actualFileName);
												}
												Utility.deleteOldDomain(this.searchClient);
												try {
													Utility.sendTextSms("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name);
												} catch (Exception e1) {
													// TODO Auto-generated catch
													// block
													e1.printStackTrace();
												}
												Utility.sendEmail("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, 0, "complete");
											} catch (Exception e2) {
												// TODO Auto-generated catch
												// block
												e2.printStackTrace();
											}

											// updating database
											try {
												boolean isInserted = Utility.insertDataIntoActivityStats("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, JobCloudMainClass.activityName, "1");

												if (isInserted) {

													System.out.println("inserted successfully");
												}
											} catch (Exception e1) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

											try {
												Utility.deleteOldMessageFromqueue("https://sqs.us-east-1.amazonaws.com/306640124653/" + Utility.actualQueueName);

											} catch (Exception e1) {
												// TODO Auto-generated catch
												// block
												e1.printStackTrace();
											}
											Utility.addDomainInfoToqueue();
											if (Utility.memcacheObj == null)
												Utility.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
											Utility.memcacheObj.set(Utility.memcacheKey, 0, Utility.searchEndpoint);
											System.out.println("Value set into the memcachekey=  " + Utility.memcacheKey);
											System.out.println("complete data by" + this.thread.getName());

											System.exit(0);
										}
									}

									System.out.println("complete data by" + this.thread.getName());

								} else {
									System.out.println("query=" + this.query);
									this.startingIndex = this.startingIndex + 100;
									if (this.startingIndex + this.offset > this.uptoWhichRecord) {
										this.offset = this.uptoWhichRecord - this.lastIndex;
										this.lastIndex = this.startingIndex + this.offset;
									} else {
										this.lastIndex = this.startingIndex + this.offset;

									}

									this.res = null;
									populatingData_toCloud();
								}
							} catch (AmazonCloudSearchInternalServerException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								this.requestList.clear();

								if (this.lastIndex >= this.uptoWhichRecord) {
									System.out.println("Thread completed named by" + this.getName());
									Utility.numberOfCompleteThreads = Utility.numberOfCompleteThreads + 1;
									if (Utility.numberOfCompleteThreads >= Utility.totalNumberOfhread) {
										if (!Utility.isThisTestrun) {

											try {
												// check that file all ready
												// written with the new record
												if (!Utility.is_new_domain_exist(Utility.actualFileName)) {
													Utility.writeDomainInfo(Utility.documentEndpoint + "|" + Utility.searchEndpoint, Utility.actualFileName);
												}
												Utility.deleteOldDomain(this.searchClient);
												try {
													Utility.sendTextSms("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name);
												} catch (Exception e1) {
													// TODO Auto-generated catch
													// block
													e1.printStackTrace();
												}
												Utility.sendEmail("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, 0, "complete");
											} catch (Exception e2) {
												// TODO Auto-generated catch
												// block
												e2.printStackTrace();
											}

											// updating database
											try {
												boolean isInserted = Utility.insertDataIntoActivityStats("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, JobCloudMainClass.activityName, "1");

												if (isInserted) {

													System.out.println("inserted successfully");
												}
											} catch (Exception e1) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

											try {
												Utility.deleteOldMessageFromqueue("https://sqs.us-east-1.amazonaws.com/306640124653/" + Utility.actualQueueName);

												Utility.addDomainInfoToqueue();
											} catch (Exception e1) {
												// TODO Auto-generated catch
												// block
												e1.printStackTrace();
											}

											if (Utility.memcacheObj == null)
												Utility.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
											Utility.memcacheObj.set(Utility.memcacheKey, 0, Utility.searchEndpoint);
											System.out.println("complete data by" + this.thread.getName());

											System.exit(0);
										}
									}

									System.out.println("complete data by" + this.thread.getName());

								} else {
									System.out.println("query=" + this.query);
									this.startingIndex = this.startingIndex + 100;
									if (this.startingIndex + this.offset > this.uptoWhichRecord) {
										this.offset = this.uptoWhichRecord - this.lastIndex;
										this.lastIndex = this.startingIndex + this.offset;
									} else {
										this.lastIndex = this.startingIndex + this.offset;

									}

									this.res = null;
									populatingData_toCloud();
								}
							}
						} else {
							this.requestList.clear();

							// from here we will again get result in new
							// limit
							if (this.lastIndex >= this.uptoWhichRecord) {
								System.out.println("Thread completed named by" + this.getName());
								Utility.numberOfCompleteThreads = Utility.numberOfCompleteThreads + 1;
								if (Utility.numberOfCompleteThreads >= Utility.totalNumberOfhread) {
									if (!Utility.isThisTestrun) {

										try {
											// check that file all ready
											// written with the new record
											if (!Utility.is_new_domain_exist(Utility.actualFileName)) {
												Utility.writeDomainInfo(Utility.documentEndpoint + "|" + Utility.searchEndpoint, Utility.actualFileName);
											}
											Utility.deleteOldDomain(this.searchClient);
											try {
												Utility.sendTextSms("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name);
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											Utility.sendEmail("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, 0, "complete");

										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										// updating database
										try {
											boolean isInserted = Utility.insertDataIntoActivityStats("Complete Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()) + " Using Table=" + this.table_name, JobCloudMainClass.activityName, "1");

											if (isInserted) {

												System.out.println("inserted successfully");
											}
										} catch (Exception e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}

										try {
											Utility.deleteOldMessageFromqueue("https://sqs.us-east-1.amazonaws.com/306640124653/" + Utility.actualQueueName);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										try {
											Utility.addDomainInfoToqueue();
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										if (Utility.memcacheObj == null)
											Utility.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));
										Utility.memcacheObj.set(Utility.memcacheKey, 0, Utility.searchEndpoint);
										System.out.println("Value set into the memcachekey=  " + Utility.memcacheKey);
										System.out.println("complete data by" + this.thread.getName());

										System.exit(0);
									}
								}

								// System.out.println("complete data by" +
								// this.thread.getName());

							} else {
								this.startingIndex = this.lastIndex;
								if (this.startingIndex + this.offset > this.uptoWhichRecord) {
									this.offset = this.uptoWhichRecord - this.lastIndex;
									this.lastIndex = this.startingIndex + this.offset;
								} else {
									this.lastIndex = this.startingIndex + this.offset;

								}

								this.res = null;
								populatingData_toCloud();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

		} catch (SQLException e) {
			throw new RuntimeException("Cannot connect the database!", e);
		} finally {
			try {
				if (this.con != null)
					this.con.close();
				this.searchClient = null;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void halfDataIsUploaded(AmazonCloudSearchClient searchClient) {

		if (Utility.dataInsertedInDomain > Utility.halfJobsData) {
			if (!Utility.isThisTestrun) {
				try {
					Utility.isHalfUploaded = true;
					// check that file all ready
					// written with the new record
					if (!Utility.is_new_domain_exist(Utility.actualFileName)) {
						Utility.writeDomainInfo(Utility.documentEndpoint + "|" + Utility.searchEndpoint, Utility.actualFileName);
					}

					try {
						Utility.sendTextSms(Utility.documentEndpoint + "|" + Utility.searchEndpoint);
					} catch (Exception e) {
						// TODO Auto-generated catch
						// block
						e.printStackTrace();
					}

					Utility.sendEmail(Utility.halfJobsData + " Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()), 0, "halfUploaded");

					Utility.deleteOldDomain(searchClient);

				} catch (Exception e2) {
					// TODO Auto-generated catch
					// block
					e2.printStackTrace();
				}

				try {
					boolean isInserted = Utility.insertDataIntoActivityStats(Utility.halfJobsData + " Data has been uploaded in " + Utility.domainName + Utility.dateFormat.format(new Date()), JobCloudMainClass.activityName, "0");

					if (isInserted) {

						System.out.println("inserted successfully");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch
					// block
					e.printStackTrace();
				}
			}
		}

	}
}
